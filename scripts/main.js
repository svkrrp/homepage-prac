const usernameInput = document.getElementById('username');
const passwordInput = document.getElementById('password');
const submitButton = document.querySelector('button');
//const form = document.querySelector('form');
const inputs = document.querySelectorAll('input');
const loginForm = document.querySelector('login-form');

const submitButtonHandler = (event) => {
  const username = usernameInput.value.trim();
  const password = passwordInput.value.trim();
  const regExPassword = /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/;
  const regExUsername = /^(?=.{8,})$/;
  if (username === '' || password === '') {
    //alert("fill all the fields");
    event.preventDefault();
    const usernameWarning = document.querySelector('.username-warning');
    const passwordWarning = document.querySelector('.password-warning');
    if (username === '' && password === '') {
      usernameWarning.classList.add('visible');
      usernameInput.classList.add('danger');
      passwordWarning.classList.add('visible');
      passwordInput.classList.add('danger');
    } else if (username === '') {
      usernameWarning.classList.add('visible');
      usernameInput.classList.add('danger');
    } else {
      passwordWarning.classList.add('visible');
      passwordInput.classList.add('danger');
    }
    return;
  } else if (regExUsername.test(username) && regExPassword.test(password)) {
    console.log('success');
  } else {
    event.preventDefault();
    console.log('invalid password');
  }
};

submitButton.addEventListener('click', submitButtonHandler);

inputs.forEach((input) => {
  input.addEventListener('focusin', (event) => {
    event.target.classList.add('focus');
  });
  input.addEventListener('focusout', (event) => {
    event.target.classList.remove('focus');
  });
});
